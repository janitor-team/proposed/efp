#!/usr/bin/perl -w

while (<>) {

	/processor/ && next;

	# "#[FOO + BAR]" ==> "#(FOO + BAR)"
	s/#\[(.*)\]/#($1)/;

	# "FOO equ BAR" ==> "FOO = BAR"
	s/^\s*(\w+)\s+equ\s+(.*)$/$1 = $2/;

	# "org FOO" ==> "*=FOO"
	s/\s*org\s+/*=/;

	# remove beginning dot (.) from labels
	s/^\.(\w+)/-$1/;
	s/\s+\.(\w+)/ $1/;

	# "FOO subroutine" ==> "FOO"
	s/^\s*(\w+)\s+subroutine/$1/;

	s/\s*SEG\.U\s+\w+/.zero/;
	s/\s*SEG\s+/./;

	s/^\s*BYTE\s+([%\$]\w+)/\.byte $1/;
	s/^\s*BYTE\s+(-?[%\$]?[0-9a-fA-F]+)/\.byte $1/;
	s/^\s*WORD\s+(-?[%\$]?[0-9a-fA-F]+)/\.word $1/;
	s/^\s*WORD\s+/.word /;
	s/^\s*BYTE\s+/.byte /;
	s/^\s*ASCII\s+("[-=\w< >]+")/\.asc $1/;
	s/^\s*DS\.B\s+(\(?[#%\$]?[0-9a-fA-F]+)/\.dsb $1/;

	# "WORD FOO" ==> "FOO .word"
	s/^\s*BYTE\s+([-\w_]+)/$1 \.byte /;
	s/^\s*WORD\s+([-\w+]+)/$1 \.word /;

	# "FOO BYTE" ==> "FOO .byte 0"
	s/(\s*\w+)\s+BYTE/$1 .byte 0/;

	# "LABEL WORD foo,bar,baz" ==> "LABEL .word foo,bar,baz"
	s/(\s*\w+)\s+WORD\s+((\w+)\s*(,\s*\w+)*\s*(;.*)?)$/$1 .word $2/;

	# "FOO WORD" ==> "FOO .word 0"
	s/(\s*\w+)\s+WORD/$1 .word 0/;

	print;
}
